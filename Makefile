pip:
	docker build -f docker/Dockerfile-pip -t adventofcode_pip .

build:
	docker build -f docker/Dockerfile -t adventofcode .

requirements: pip
	docker run -it --rm -v "$$(pwd):/app" adventofcode_pip:latest pip-compile --annotate -U /app/requirements.in -v -o  /app/requirements.txt
	docker run -it --rm -v "$$(pwd):/app" adventofcode_pip:latest pip-compile --annotate -U /app/requirements-dev.in -v -o  /app/requirements-dev.txt
