#!/bin/bash
# build docker container with: make build
docker run -it --rm -v "$(pwd):/app" adventofcode:latest ipython -- $@
