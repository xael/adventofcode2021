# Advent of Code 2021

Done in python 3.10+

Code is under code/ folder.

# Install 

Need one external lib for parsing command line. Install it with :

    pip install -r requirements.txt

# Run

To run code against given test case :

    python code/dayX.py -t 
    
To run code against real data :

    python code/dayX.py
    
There is a debug mode with `-d` option.

## Run in docker

Build docker image:

    make build
    
Run:

    ./run_code.sh code/dayX.py

# Commit

Install [pre-commit](https://pre-commit.com/) to run black and flake8 before commiting.

Docker image `xael/docker-precommit_python:latest` embeds flake8 and black with my prefered plugings
and custom configuration.
