# -*- coding: utf-8 -*-
"""Advent of code 2021, day 03

Usage:
   day03.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""
import logging
import sys
import unittest
from collections import Counter

from docopt import docopt


TEST_DATA_FILENAME = "data/day_03_example.txt"
FULL_DATA_FILENAME = "data/day_03.txt"


class InstructionReader:
    def __init__(self, filename: str):
        with open(filename, "r") as handle:
            self.data = handle.read().strip().split("\n")

    def read(self):
        return self.data


class BinaryDiagnostic:
    def __init__(self, filename: str):
        self.instruction = InstructionReader(filename=filename)

    def verify(self):
        instructions = self.instruction.read()
        most_common_bit = []
        least_common_bit = []
        for i in range(len(instructions[0])):
            bits = Counter([x[i] for x in instructions])
            most_common_bit.append(max(bits, key=lambda key: bits[key]))
            least_common_bit.append(min(bits, key=lambda key: bits[key]))

        gamma_rate = int("".join(most_common_bit), 2)
        epsilon_rate = int("".join(least_common_bit), 2)

        return gamma_rate * epsilon_rate


class LifeSupportRating:
    def __init__(self, filename: str):
        self.instructions = InstructionReader(filename=filename)

    def verify(self):
        oxygene_generator_rating = self._get_oxygene_generator_rating()
        cO2_scrubber_rating = self._get_cO2_scrubber_rating()
        return oxygene_generator_rating * cO2_scrubber_rating

    def _get_oxygene_generator_rating(self):
        return self._get_final_value(get_maximum=True)

    def _get_cO2_scrubber_rating(self):
        return self._get_final_value(get_maximum=False)

    def _get_final_value(self, get_maximum):
        data = self.instructions.read()
        for pos in range(len(data[0])):
            bits = self._get_bit_count_at_position(pos=pos, data=data)
            maximum = max(bits, key=lambda key: bits[key])
            minimum = min(bits, key=lambda key: bits[key])
            if bits[maximum] == bits[minimum]:
                bit_to_keep = "1" if get_maximum else "0"
            else:
                bit_to_keep = maximum if get_maximum else minimum

            filtered_data = [x for x in data if x[pos] == bit_to_keep]
            if len(filtered_data) == 1:
                return int(filtered_data[0], 2)
            else:
                data = filtered_data

    def _get_bit_count_at_position(self, pos: int, data: list):
        bits = Counter([x[pos] for x in data])
        return bits


class BinaryDiagnosticTestCase(unittest.TestCase):
    def test_example_part_1(self):
        binary_diagnostic = BinaryDiagnostic(filename=TEST_DATA_FILENAME)
        result = binary_diagnostic.verify()
        assert result == 198

    def test_example_part_2(self):
        life_support_rating = LifeSupportRating(filename=TEST_DATA_FILENAME)
        result = life_support_rating.verify()
        assert result == 230


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        binary_diagnostic = BinaryDiagnostic(FULL_DATA_FILENAME)
        result = binary_diagnostic.verify()

        logging.info(f"Part 1 result found: {result}")

        life_support_rating = LifeSupportRating(FULL_DATA_FILENAME)
        result = life_support_rating.verify()

        logging.info(f"Part 1 result found: {result}")
