# -*- coding: utf-8 -*-
"""Advent of code 2021, day 10

Usage:
   day10.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import re
import sys
import unittest

from docopt import docopt

TEST_DATA_FILENAME = "data/day_10_example.txt"
FULL_DATA_FILENAME = "data/day_10.txt"


class SyntaxScoringReader:
    def __init__(self, filename):
        logging.debug(f"SyntaxScoring:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")

    @property
    def items(self):
        for item in self.data:
            yield item.strip()


class SyntaxScoring:
    def __init__(self, filename: str):
        self.instruction = SyntaxScoringReader(filename=filename)

    def run(self):
        score = 0
        score_incomplete = []
        for line in self.instruction.items:
            try:
                self.analyse_string(line)
            except ValueError as e:
                wrong_chunk = e.args[0]
                score += self.get_final_score(wrong_chunk)
                continue

            # line is just incomplete
            try:
                self.incomplete_string(line)
            except ValueError as e:
                incomplete_line = e.args[0]
                print(incomplete_line)
                score_incomplete.append(self.score_for_completing_line(incomplete_line))

        score_incomplete.sort()
        middle_score_incomplete = score_incomplete[int(len(score_incomplete) / 2)]
        return score, middle_score_incomplete

    def score_for_completing_line(self, line):
        points = {
            "(": 1,
            "[": 2,
            "{": 3,
            "<": 4,
        }
        map_points = [points[x] for x in line]
        score = 0
        for i in range(len(map_points) - 1, -1, -1):
            score = score * 5 + map_points[i]

        return score

    def analyse_string(self, line):
        regex_short = r"\<\>|\[\]|\(\)|\{\}"  # NOQA: FS003
        if re.search(regex_short, line) is None:
            opening = r"\[|\(|\<|\{"
            line = re.sub(opening, "", line)
            if len(line) == 0:
                return
            else:
                raise ValueError(line[0])

        new_line = "".join(re.split(regex_short, line))
        if len(new_line) > 0:
            self.analyse_string(line=new_line)

    def incomplete_string(self, line):
        regex_short = r"\<\>|\[\]|\(\)|\{\}"  # NOQA: FS003
        if re.search(regex_short, line) is None:
            raise ValueError(line)

        new_line = "".join(re.split(regex_short, line))
        if len(new_line) > 0:
            self.incomplete_string(line=new_line)

    @staticmethod
    def get_final_score(wrong_chunk):
        return {
            ")": 3,
            "]": 57,
            "}": 1197,
            ">": 25137,
        }[wrong_chunk]


class SyntaxScoringTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = SyntaxScoring(filename=TEST_DATA_FILENAME)
        result, result_incomplet = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 26397

    def test_example_part_2(self):
        runner = SyntaxScoring(filename=TEST_DATA_FILENAME)
        result, result_incomplete = runner.run()
        logging.info(f"Test Part 2: {result_incomplete}")
        assert result_incomplete == 288957


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = SyntaxScoring(FULL_DATA_FILENAME)
        result, result_incomplete = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = SyntaxScoring(FULL_DATA_FILENAME)
        result, result_incomplete = runner.run()

        logging.info(f"Part 2: {result_incomplete}")
