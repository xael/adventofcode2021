# -*- coding: utf-8 -*-
"""Advent of code 2021, day 08

Usage:
   day08.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt

TEST_DATA_FILENAME = "data/day_08_example.txt"
FULL_DATA_FILENAME = "data/day_08.txt"


class Segments:
    @classmethod
    def digits(cls):
        return {
            1: "cf",
            7: "acf",
            4: "bcdf",
            2: "acdeg",
            3: "acdfg",
            5: "abdfg",
            0: "abcefg",
            6: "abdefg",
            9: "abcdfg",
            8: "abcdefg",
        }

    @classmethod
    def digits_by_length(cls):
        length = {x: len(cls.digits()[x]) for x in cls.digits()}
        result = {x: [] for x in range(2, 8)}
        for i in range(2, 8):
            for digit in length:
                if length[digit] == i:
                    result[i].append(digit)
        return result

    @classmethod
    def segments(cls):
        return {v: k for k, v in cls.digits.items()}


class SevenSegmentReader:
    def __init__(self, filename):
        logging.debug(f"SevenSegment:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")

    @property
    def items(self):
        for item in self.data:
            yield self._format_item(item)

    @staticmethod
    def _format_item(item):
        patterns, values = item.split("|")
        patterns = patterns.strip().split(" ")
        values = values.strip().split(" ")
        return (patterns, values)


def alpha_sort(value):
    a = list(value)
    a.sort()
    return "".join(a)


class SevenSegment:
    def __init__(self, filename: str):
        self.instruction = SevenSegmentReader(filename=filename)

    def run(self):
        counter = 0
        for _patterns, values in self.instruction.items:
            for value in values:
                if self.find_easy_digits(value) is not None:
                    counter += 1

        return counter

    def find_easy_digits(self, value):
        easy_digits = {
            x: Segments.digits_by_length()[x]
            for x in Segments.digits_by_length()
            if len(Segments.digits_by_length()[x]) == 1
        }
        if len(value) in easy_digits.keys():
            return easy_digits[len(value)]
        return None


class SevenSegmentDecoder(SevenSegment):
    def __init__(self, filename: str):
        self.instruction = SevenSegmentReader(filename=filename)

    def run(self):
        counter = 0
        digits_by_length = Segments.digits_by_length()
        for patterns, values in self.instruction.items:
            digits = {}
            not_sure = {}
            for pattern in patterns:
                if len(digits_by_length[len(pattern)]) == 1:
                    # Easy digits: only one occurence
                    digits[digits_by_length[len(pattern)][0]] = pattern
                else:
                    not_sure[pattern] = digits_by_length[len(pattern)]

            logging.debug(f"Digits: {digits}")
            logging.debug(f"Not sure: {not_sure}")

            c_or_f = list(digits[1])

            six_in = [x for x in not_sure if 6 in not_sure[x]]

            digits[6] = self.not_in(digits[7], six_in)[0]

            zero_in = [x for x in not_sure if 0 in not_sure[x] and x != digits[6]]
            digits[0] = self.not_in(digits[4], zero_in)[0]

            digits[9] = [
                x
                for x in not_sure
                if 9 in not_sure[x] and x != digits[6] and x != digits[0]
            ][0]

            three_in = [x for x in not_sure if 3 in not_sure[x]]
            digits[3] = self.is_in(digits[1], three_in)[0]

            # c is not in 6 but in 2
            c = [x for x in c_or_f if x not in digits[6]][0]
            digits[2] = [
                x for x in not_sure if 2 in not_sure[x] and c in x and x != digits[3]
            ][0]
            digits[5] = [
                x
                for x in not_sure
                if 5 in not_sure[x] and x != digits[2] and x != digits[3]
            ][0]

            logging.debug(f"Digits: {digits}")

            ordered_digits = {alpha_sort(digits[x]): x for x in digits}
            number = []
            for value in values:
                value = alpha_sort(value)
                number.append(str(ordered_digits[value]))

            counter += int("".join(number))

        return counter

    def is_in(self, value, items):
        is_in = []
        for item in items:
            if set(value).issubset(item):
                is_in.append(item)

        return is_in

    def not_in(self, value, items):
        not_in = []
        for item in items:
            if not set(value).issubset(item):
                not_in.append(item)

        return not_in

    def difference_between(self, seg1, seg2):
        diff = []
        for c in seg1:
            if c not in seg2:
                diff.append(c)

        return diff


class SevenSegmentTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = SevenSegment(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 26

    def test_example_part_2(self):
        runner = SevenSegmentDecoder(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 2: {result}")
        assert result == 61229 + 5353


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = SevenSegment(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = SevenSegmentDecoder(FULL_DATA_FILENAME)
        result = runner.run()

        logging.info(f"Part 2: {result}")
