# -*- coding: utf-8 -*-
"""Advent of code 2021, day 04

Usage:
   day04.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt

TEST_DATA_FILENAME = "data/day_04_example.txt"
FULL_DATA_FILENAME = "data/day_04.txt"


class BoardWinsException(Exception):
    pass


class BingoBoard:
    def __init__(self, values: list):
        def convert_to_int(values):
            return [int(x) for x in values]

        self.values = [convert_to_int(y.split()) for y in values.split("\n")]
        self.values = [[int(x) for x in y.split()] for y in values.split("\n")]
        self.checked = [[False for x in self.grid_colums] for y in self.grid_rows]
        self.last_draw = None

    @property
    def grid_colums(self):
        return range(len(self.values[0]))

    @property
    def grid_rows(self):
        return range(len(self.values))

    def draw(self, number: int):
        for x in self.grid_colums:
            for y in self.grid_rows:
                if self.values[y][x] == number:
                    self.checked[y][x] = True

        self.last_draw = number
        self.verify_grid()

    def verify_grid(self):
        for y in self.grid_rows:
            if False not in self.checked[y]:
                raise BoardWinsException

        for x in self.grid_colums:
            if False not in [self.checked[y][x] for y in self.grid_rows]:
                raise BoardWinsException

    def get_score(self):
        grid = [
            [
                self.values[y][x] if self.checked[y][x] is False else 0
                for y in self.grid_rows
            ]
            for x in self.grid_colums
        ]
        score = sum(sum(x) for x in grid) * self.last_draw
        return score


class BingoGameReader:
    def __init__(self, filename):
        with open(filename, "r") as data_file:
            data = data_file.read().strip().split("\n\n")
            self.drawns = [int(x) for x in data[0].split(",")]
            self.boards_data = data[1:]

    @property
    def drawn(self):
        for number in self.drawns:
            yield number

    @property
    def boards(self):
        for board in self.boards_data:
            yield board


class BingoGame:
    def __init__(self, filename: str):
        bgr = BingoGameReader(filename=filename)
        self.boards = []
        for board in bgr.boards:
            self.boards.append(BingoBoard(board))

        self.drawns = bgr.drawn

    def run(self):
        for drawn in self.drawns:
            for board in self.boards:
                try:
                    board.draw(number=drawn)
                except BoardWinsException:
                    return board.get_score()

        return None


class BingoGameLetTheSquidWin(BingoGame):
    def run(self):
        winning_boards = []
        for drawn in self.drawns:
            for board in self.boards:
                if board not in winning_boards:
                    try:
                        board.draw(number=drawn)
                    except BoardWinsException:
                        winning_boards.append(board)

        return winning_boards[-1].get_score()


class BingoGameTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = BingoGame(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 4512

    def test_example_part_2(self):
        runner = BingoGameLetTheSquidWin(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 2: {result}")
        assert result == 1924


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = BingoGame(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = BingoGameLetTheSquidWin(FULL_DATA_FILENAME)
        result = runner.run()

        logging.info(f"Part 2: {result}")
