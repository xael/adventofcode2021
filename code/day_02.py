# -*- coding: utf-8 -*-
"""Advent of code 2021, day 02

Usage:
   day02.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""

import logging
import sys
import unittest
from dataclasses import dataclass

from docopt import docopt


TEST_DATA_FILENAME = "data/day_02_example.txt"
FULL_DATA_FILENAME = "data/day_02.txt"


@dataclass
class Command:
    orientation: str
    quantity: int


@dataclass
class Vector:
    vertical: int
    horizontal: int

    def apply_quantity(self, quantity):
        self.vertical = self.vertical * quantity
        self.horizontal = self.horizontal * quantity


@dataclass
class ImprovedVector:
    aim: int
    vertical: int
    horizontal: int

    def apply_quantity(self, quantity):
        self.vertical = self.aim * quantity
        self.horizontal = self.horizontal * quantity


@dataclass
class Position:
    depth: int = 0
    horizontal: int = 0

    def apply_vector(self, vector: Vector):
        self.depth += vector.vertical
        self.horizontal += vector.horizontal

    def get_position(self):
        return self.depth * self.horizontal


@dataclass
class ImprovedPosition(Position):
    aim: int = 0

    def apply_vector(self, vector: Vector, aim):
        if vector.vertical != 0:
            self.aim += vector.vertical

        if vector.horizontal != 0:
            self.horizontal += vector.horizontal
            self.depth += aim * vector.horizontal


class InstructionReader:
    def __init__(self, filename: str):
        with open(filename, "r") as handle:
            self.data = handle.read().strip().split("\n")

    def read(self):
        for item in self.data:
            orientation, quantity = item.split()
            quantity = int(quantity)
            vector = self._translate_command_to_vector(
                command=Command(orientation=orientation, quantity=quantity)
            )
            yield vector

    @staticmethod
    def _translate_command_to_vector(command: Command):
        vector = {
            "forward": Vector(vertical=0, horizontal=1),
            "up": Vector(vertical=-1, horizontal=0),
            "down": Vector(vertical=1, horizontal=0),
        }
        vector = vector[command.orientation]
        vector.apply_quantity(quantity=command.quantity)
        return vector


class Submarine:
    def __init__(self, filename: str):
        self.instruction = InstructionReader(filename=filename)
        self.position = Position(depth=0, horizontal=0)

    def follow_route(self):
        for vector in self.instruction.read():
            self.position.apply_vector(vector)

        return self.position


class ImprovedSubmarine:
    def __init__(self, filename: str):
        self.instruction = InstructionReader(filename=filename)
        self.position = ImprovedPosition(aim=0, depth=0, horizontal=0)

    def follow_route(self):
        for vector in self.instruction.read():
            self.position.apply_vector(vector=vector, aim=self.position.aim)

        return self.position


class SubmarineTestCase(unittest.TestCase):
    def test_example_part_1(self):
        submarine = Submarine(filename=TEST_DATA_FILENAME)
        result = submarine.follow_route()
        assert result.get_position() == 150

    def test_example_part_2(self):
        submarine = ImprovedSubmarine(filename=TEST_DATA_FILENAME)
        result = submarine.follow_route()
        assert result.get_position() == 900


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        submarine = Submarine(FULL_DATA_FILENAME)
        result = submarine.follow_route()

        logging.info(f"Part 1 result found: {result.get_position()}")

        submarine = ImprovedSubmarine(FULL_DATA_FILENAME)
        result = submarine.follow_route()

        logging.info(f"Part 2 result found: {result.get_position()}")
