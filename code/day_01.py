# -*- coding: utf-8 -*-
"""Advent of code 2021, day 01

Usage:
   day01.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest
from enum import Enum

from docopt import docopt


class SonarState(Enum):
    NA = "N/A"
    INCREASE = "increase"
    DECREASE = "decrease"
    SAME = "same"


class SonarSweepReader:
    def __init__(self, filename):
        with open(filename, "r") as handle:
            self.data = handle.read().split()

    def read(self):
        previous_measure = None
        for item in self.data:
            data = int(item)
            yield self._compare_measure(previous=previous_measure, current=data)
            previous_measure = data

    @staticmethod
    def _compare_measure(previous, current):
        if previous is None:
            return SonarState.NA

        if previous < current:
            return SonarState.INCREASE

        if previous > current:
            return SonarState.DECREASE

        if previous == current:
            return SonarState.SAME


class ImprovedSonarSweepReader(SonarSweepReader):
    def read(self):
        measure_window = []

        for item in self.data:
            current_measure = int(item)
            if len(measure_window) < 3:
                measure_window.append(current_measure)
                yield SonarState.NA
                continue

            previous_sum_measure = sum(measure_window)
            measure_window.pop(0)
            measure_window.append(current_measure)
            current_sum_measure = sum(measure_window)

            yield self._compare_measure(
                previous=previous_sum_measure, current=current_sum_measure
            )


class SonarSweep:
    def __init__(self, filename, sonar_sweep=SonarSweepReader):
        self.data_from_sonar = sonar_sweep(filename)

    def analyse_data(self):
        analyse = {SonarState.__members__[x]: 0 for x in SonarState.__members__}
        for data in self.data_from_sonar.read():
            analyse[data] += 1

        return analyse


class SonarSweepTestCase(unittest.TestCase):
    def test_example_part_1(self):
        ss = SonarSweep("data/day_01_example.txt")
        result = ss.analyse_data()
        assert result[SonarState.INCREASE] == 7

    def test_example_part_2(self):
        ss = SonarSweep("data/day_01_example.txt", sonar_sweep=ImprovedSonarSweepReader)
        result = ss.analyse_data()
        assert result[SonarState.INCREASE] == 5


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        ss = SonarSweep("data/day_01.txt")
        result = ss.analyse_data()

        logging.info(f"Part 1 result found: {result}")

        ss = SonarSweep("data/day_01.txt", sonar_sweep=ImprovedSonarSweepReader)
        result = ss.analyse_data()

        logging.info(f"Part 2 result found: {result}")
