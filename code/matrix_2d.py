from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int


class Matrix2d:
    def __init__(
        self, max_x: int, max_y: int, default: any = 0, use_diagonal: bool = False
    ):
        self.data = []
        self.max_x = max_x
        self.max_y = max_y
        self.use_diagonal = use_diagonal
        for _ in range(max_x * max_y):
            self.data.append(default)

    def get(self, point: Point):
        return self.data[self.index(point)]

    def set(self, point: Point, value: any):  # NOQA: A003
        self.data[self.index(point)] = value

    def set_all(self, values: list):
        for x in range(self.max_x):
            for y in range(self.max_y):
                point = Point(x=x, y=y)
                self.set(point, value=values[self.index(point)])

    def neighbors(self, point: Point):
        neighbors = []
        for x in range(-1, 2):
            for y in range(-1, 2):
                if (not self.use_diagonal and x == y) or (x == 0 and y == 0):
                    continue
                if self._is_in_matrix(Point(x=point.x + x, y=point.y + y)):
                    neighbors.append(Point(x=point.x + x, y=point.y + y))

        return neighbors

    def all_coordinates(self):
        points = []
        for x in range(self.max_x):
            for y in range(self.max_y):
                points.append(Point(x=x, y=y))

        return points

    def index(self, point: Point):
        return point.x + point.y * self.max_x

    def _is_in_matrix(self, point: Point):
        return 0 <= point.x < self.max_x and 0 <= point.y < self.max_y

    def display(self):
        print("-" * self.max_x * 3)
        for y in range(self.max_y):
            print(
                "".join(
                    [
                        str(x)
                        for x in self.data[y * self.max_x : y * self.max_x + self.max_x]
                    ]
                )
            )

    @property
    def number_of_elements(self):
        return self.max_x * self.max_y
