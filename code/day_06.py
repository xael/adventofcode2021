# -*- coding: utf-8 -*-
"""Advent of code 2021, day 06

Usage:
   day06.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt


TEST_DATA_FILENAME = "data/day_06_example.txt"
FULL_DATA_FILENAME = "data/day_06.txt"


class LanterFishReader:
    def __init__(self, filename):
        logging.debug(f"LanterFish:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split(",")

    @property
    def fishs(self):
        for fish in self.data:
            yield int(fish)


class LanterFish:
    def __init__(self, filename: str, number_of_days: int = 80):
        self.number_of_days = number_of_days
        self.initial_population = LanterFishReader(filename=filename)
        self.population_by_days = [0 for x in range(0, number_of_days + 10)]

        for fish in self.initial_population.fishs:
            self.place_fish(day=0, fish=fish)

    def place_fish(self, day, fish):
        self.population_by_days[day + fish] += 1

    def run(self):
        for day in range(0, self.number_of_days):
            fishs_creating_a_new_fish = self.population_by_days[day]
            self.population_by_days[day] = 0
            self.population_by_days[day + 7] += fishs_creating_a_new_fish
            # New fishs
            self.population_by_days[day + 9] += fishs_creating_a_new_fish
            logging.debug(f"day: {day+1} -> {sum(self.population_by_days)}")

        return sum(self.population_by_days)


class LanterFishTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = LanterFish(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 5934

    def test_example_part_2(self):
        runner = LanterFish(filename=TEST_DATA_FILENAME, number_of_days=256)
        result = runner.run()
        logging.info(f"Test Part 2: {result}")
        assert result == 26984457539


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = LanterFish(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = LanterFish(FULL_DATA_FILENAME, number_of_days=256)
        result = runner.run()

        logging.info(f"Part 2: {result}")
