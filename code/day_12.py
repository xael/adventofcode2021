# -*- coding: utf-8 -*-
"""Advent of code 2021, day 12

Usage:
   day12.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest
from dataclasses import dataclass, field

from docopt import docopt

TEST_DATA_FILENAME = "data/day_12_example.txt"
FULL_DATA_FILENAME = "data/day_12.txt"


@dataclass
class Node:
    name: str
    has_been_visited: int = 0
    links: list = field(default_factory=list)

    def __repr__(self):
        return self.name

    def mark_visited(self):
        self.has_been_visited += 1

    @property
    def is_start(self):
        return self.name == "start"

    @property
    def is_end(self):
        return self.end == "end"

    @property
    def is_small(self):
        return self.name == self.name.lower()

    @property
    def can_be_visited(self):
        if self.is_small:
            return self.has_been_visited == 0

        return True

    def add_link(self, link: str):
        self.links.append(link)


@dataclass
class NodePart2(Node):
    def can_be_visited(self, nodes):
        visited = [nodes[x].has_been_visited for x in nodes]
        if self.is_small:
            if 2 not in visited:
                return self.has_been_visited < 2

            return self.has_been_visited == 0

        return True


class PassagePathingReader:
    def __init__(self, filename):
        logging.debug(f"PassagePathing:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip().split("\n")

    @property
    def items(self):
        for item in self.data:
            yield self._format_item(item)

    @staticmethod
    def _format_item(item):
        return item.split("-")


class PassagePathing:
    def __init__(self, filename: str):
        self.instruction = PassagePathingReader(filename=filename)
        self.nodes = {}

    def run(self):
        self.construct_tree()
        paths = self.explore_path(start=self.nodes["start"])
        for path in paths:
            logging.debug(",".join([x.name for x in path]))

        return len(paths)

    def construct_tree(self):
        for node_a, node_b in self.instruction.items:
            if node_a not in self.nodes:
                self.nodes[node_a] = self.create_node(name=node_a)
            if node_b not in self.nodes:
                self.nodes[node_b] = self.create_node(name=node_b)

            self.nodes[node_a].add_link(node_b)
            self.nodes[node_b].add_link(node_a)

    def create_node(self, name):
        return Node(name=name)

    def explore_path(self, start, path=None):
        if path is None:
            path = []

        start.mark_visited()
        path = path + [start]
        if start.name == "end":
            return [path]

        paths = []
        visitable_nodes = self.get_visitable_nodes(links=start.links)

        saved_has_been_visited = {x: self.nodes[x].has_been_visited for x in self.nodes}
        logging.debug(
            f"visitable from {start.name}: {[x.name for x in visitable_nodes]}"
        )
        for node in visitable_nodes:
            for n in saved_has_been_visited:
                self.nodes[n].has_been_visited = saved_has_been_visited[n]

            for newpath in self.explore_path(start=node, path=path):
                paths.append(newpath)

        return paths

    def get_visitable_nodes(self, links):
        visitable_nodes = [
            self.nodes[x]
            for x in links
            if self.nodes[x].can_be_visited and x != "start"
        ]
        return visitable_nodes


class PassagePathingPart2(PassagePathing):
    def create_node(self, name):
        return NodePart2(name=name)

    def get_visitable_nodes(self, links):
        visitable_nodes = [
            self.nodes[x]
            for x in links
            if self.nodes[x].can_be_visited(self.nodes) and x != "start"
        ]
        return visitable_nodes


class PassagePathingTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = PassagePathing(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 10

    def test_example_part_2(self):
        runner = PassagePathingPart2(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 2: {result}")
        assert result == 36


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = PassagePathing(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = PassagePathingPart2(FULL_DATA_FILENAME)
        result = runner.run()

        logging.info(f"Part 2: {result}")
