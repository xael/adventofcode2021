# -*- coding: utf-8 -*-
"""Advent of code 2021, day 05

Usage:
   day05.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest
from collections import Counter

from docopt import docopt

TEST_DATA_FILENAME = "data/day_05_example.txt"
FULL_DATA_FILENAME = "data/day_05.txt"


class HydrotermalVentsMap:
    def __init__(self, max_x, max_y):
        logging.debug(f"HydrotermalVentsMap init x:{max_x}, y:{max_y}")
        self.map_vents = [[0 for x in range(max_x + 1)] for y in range(max_y + 1)]

    def add_line(self, x1, y1, x2, y2, use_diagonal=False):
        logging.debug(f"HydrotermalVentsMap add_line {x1}, {y1} -> {x2}, {y2}")

        dx = x2 - x1
        dy = y2 - y1

        x_range = list(range(x1, x2 + (1 if dx >= 0 else -1), 1 if dx >= 0 else -1))
        y_range = list(range(y1, y2 + (1 if dy >= 0 else -1), 1 if dy >= 0 else -1))

        if dx == 0:
            logging.debug("Horizontal")
            points = list(zip([x1] * len(y_range), y_range))

        elif dy == 0:
            logging.debug("Vertical")
            points = list(zip(x_range, [y1] * len(x_range)))

        elif use_diagonal and abs(dx) == abs(dy):
            logging.debug("Diagonal")
            points = list(zip(x_range, y_range))

        else:
            logging.debug("Ignored")
            return

        for x, y in points:
            self.map_vents[y][x] += 1

    def count_overlap(self):
        counters = [
            Counter(x)
            for x in [list(filter(lambda x: x > 1, b)) for b in self.map_vents]
            if x
        ]
        result = sum(sum(c.values()) for c in counters)
        return result


class HydrotermalVentsReader:
    def __init__(self, filename):
        logging.debug(f"HydrotermalVents:init:{filename}")
        self.data = []
        with open(filename, "r") as data_file:
            for line in data_file.read().strip().split("\n"):
                self.data.append(
                    [[int(y) for y in x.split(",")] for x in line.split(" -> ")]
                )

    @property
    def items(self):
        for item in self.data:
            yield item

    @property
    def max_x(self):
        data = []
        for line in self.data:
            data.append(max(line[0][0], line[1][0]))

        return max(data)

    @property
    def max_y(self):
        data = []
        for line in self.data:
            data.append(max(line[0][1], line[1][1]))

        return max(data)


class HydrotermalVents:
    def __init__(self, filename: str):
        self.map_data = HydrotermalVentsReader(filename=filename)

    def run(self, use_diagonal=False):
        vents_map = HydrotermalVentsMap(
            max_x=self.map_data.max_x, max_y=self.map_data.max_y
        )
        for line in self.map_data.items:
            vents_map.add_line(
                x1=line[0][0],
                y1=line[0][1],
                x2=line[1][0],
                y2=line[1][1],
                use_diagonal=use_diagonal,
            )
        return vents_map.count_overlap()


class HydrotermalVentsTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = HydrotermalVents(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 5

    def test_example_part_2(self):
        runner = HydrotermalVents(filename=TEST_DATA_FILENAME)
        result = runner.run(use_diagonal=True)
        logging.info(f"Test Part 2: {result}")
        assert result == 12


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = HydrotermalVents(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = HydrotermalVents(FULL_DATA_FILENAME)
        result = runner.run(use_diagonal=True)

        logging.info(f"Part 2: {result}")
