# -*- coding: utf-8 -*-
"""Advent of code 2021, day 13

Usage:
   day13.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt

from matrix_2d import Matrix2d, Point


TEST_DATA_FILENAME = "data/day_13_example.txt"
FULL_DATA_FILENAME = "data/day_13.txt"


class TransparentOrigamiReader:
    def __init__(self, filename):
        logging.debug(f"TransparentOrigami:init:{filename}")
        with open(filename, "r") as data_file:
            self.coord, self.folding = data_file.read().strip().split("\n\n")

    @property
    def folding_data(self):
        for line in self.folding.strip().split("\n"):
            axis, place = line.replace("fold along ", "").split("=")
            yield [axis, int(place)]

    @property
    def matrix_data(self):
        return [
            [int(x), int(y)] for x, y in [x.split(",") for x in self.coord.split("\n")]
        ]


class TransparentOrigami:
    def __init__(self, filename: str):
        instruction = TransparentOrigamiReader(filename=filename)

        self.data = instruction.matrix_data
        self.folding_data = list(instruction.folding_data)

        logging.debug(f"data : {self.data}")

    def run(self):
        self.fold(count=1)
        return len(self.data)

    def fold(self, count=None):
        for axis, folding_place in self.folding_data:
            folded = []

            if axis == "y":
                f_x = self.noop
                f_y = self.transform_coord_by_folding

            if axis == "x":
                f_x = self.transform_coord_by_folding
                f_y = self.noop

            for x, y in self.data:
                x = f_x(x, folding_place)
                y = f_y(y, folding_place)

                if [x, y] not in folded:
                    folded.append([x, y])

            self.data = folded

            if count is not None:
                count -= 1
                if count == 0:
                    return

        return

    def noop(self, value, folding_place):
        return value

    def transform_coord_by_folding(self, value, folding_place):
        if value > folding_place:
            return 2 * folding_place - value
        return value

    def display_matrix(self):
        max_x = max(x for x, _ in self.data) + 1
        max_y = max(y for _, y in self.data) + 1
        matrix = Matrix2d(
            max_x=max_x,
            max_y=max_y,
            default=".",
        )
        for x, y in self.data:
            matrix.set(Point(x=x, y=y), value="X")

        matrix.display()
        print("-> ", len(self.data))


class TransparentOrigamiPart2(TransparentOrigami):
    def run(self):
        self.fold()
        self.display_matrix()
        return


class TransparentOrigamiTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = TransparentOrigami(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 17


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = TransparentOrigami(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = TransparentOrigamiPart2(FULL_DATA_FILENAME)
        result = runner.run()

        logging.info(f"Part 2: {result}")
