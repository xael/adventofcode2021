# -*- coding: utf-8 -*-
"""Advent of code 2021, day 07

Usage:
   day07.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt

TEST_DATA_FILENAME = "data/day_07_example.txt"
FULL_DATA_FILENAME = "data/day_07.txt"


class CrabsReader:
    def __init__(self, filename):
        logging.debug(f"Crabs:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = [int(x) for x in data_file.read().strip().split(",")]

    @property
    def crabs(self):
        for item in self.data:
            yield item

    @property
    def max_pos(self):
        return max(self.data)

    @property
    def min_pos(self):
        return min(self.data)


class Crabs:
    def __init__(self, filename: str):
        self.map_data = CrabsReader(filename=filename)

    def run(self, use_diagonal=False):

        min_fuel = 0
        best_postition = None
        for position in range(self.map_data.min_pos, self.map_data.max_pos + 1):
            current_fuel = 0
            interrupt = False
            for crab in self.map_data.crabs:
                current_fuel += self.calculate_fuel(
                    crab_position=crab, destination=position
                )
                if best_postition is not None and current_fuel > min_fuel:
                    interrupt = True
                    break

            if not interrupt:
                min_fuel = current_fuel
                best_postition = position

        return min_fuel

    def calculate_fuel(self, crab_position, destination):
        return abs(destination - crab_position)


class CrabsV2(Crabs):
    def calculate_fuel(self, crab_position, destination):
        distance = abs(destination - crab_position)
        cost = 0
        for i in range(0, distance):
            cost += i + 1

        return cost


class CrabsTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = Crabs(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 37

    def test_example_part_2(self):
        runner = CrabsV2(filename=TEST_DATA_FILENAME)
        result = runner.run(use_diagonal=True)
        logging.info(f"Test Part 2: {result}")
        assert result == 168


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = Crabs(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = CrabsV2(FULL_DATA_FILENAME)
        result = runner.run(use_diagonal=True)

        logging.info(f"Part 2: {result}")
