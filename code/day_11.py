# -*- coding: utf-8 -*-
"""Advent of code 2021, day 11

Usage:
   day11.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest
from dataclasses import dataclass

from docopt import docopt

from matrix_2d import Matrix2d


TEST_DATA_FILENAME = "data/day_11_example.txt"
FULL_DATA_FILENAME = "data/day_11.txt"


@dataclass
class FlashCounter:
    count: int = 0

    def add_one(self):
        self.count += 1


class DumboOctopusReader:
    def __init__(self, filename):
        logging.debug(f"DumboOctopus:init:{filename}")
        with open(filename, "r") as data_file:
            self.data = data_file.read().strip()

    @property
    def matrix_data(self):
        return [int(x) for x in self.data.replace("\n", "")]

    @property
    def max_x(self):
        return len(self.data.split("\n")[0])

    @property
    def max_y(self):
        return len(self.data.split("\n"))


class DumboOctopus:
    def __init__(self, filename: str):
        self.instruction = DumboOctopusReader(filename=filename)
        self.matrix = Matrix2d(
            max_x=self.instruction.max_x,
            max_y=self.instruction.max_y,
            use_diagonal=True,
        )
        self.matrix.set_all(values=self.instruction.matrix_data)
        self.flash_counter = FlashCounter()

    def run(self):
        for _steps in range(100):
            self.run_cycle()

        return self.flash_counter.count

    def run_cycle(self):
        for point in self.matrix.all_coordinates():
            self.increase_energy_level_without_flash(point)

        for point in self.matrix.all_coordinates():
            self.test_if_flash(point)

    def test_if_flash(self, point):
        energy_level = self.matrix.get(point)
        if energy_level > 9:
            self.flash_point(point)

    def increase_energy_level_without_flash(self, point):
        energy_level = self.matrix.get(point)
        self.matrix.set(point=point, value=energy_level + 1)

    def increase_energy_level(self, point):
        energy_level = self.matrix.get(point)
        if energy_level == 0:
            return

        energy_level += 1
        if energy_level > 9:
            self.flash_point(point)
        else:
            self.matrix.set(point=point, value=energy_level)

    def flash_point(self, point):
        self.flash_counter.add_one()
        self.matrix.set(point=point, value=0)
        for point in self.matrix.neighbors(point):
            self.increase_energy_level(point)


class DumboOctopusPart2(DumboOctopus):
    def run(self):
        step = 0
        while self.flash_counter.count != self.matrix.number_of_elements:
            self.flash_counter.count = 0
            self.run_cycle()
            step += 1

        return step


class DumboOctopusTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = DumboOctopus(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 1656

    def test_example_part_2(self):
        runner = DumboOctopusPart2(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 2: {result}")
        assert result == 195


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = DumboOctopus(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = DumboOctopusPart2(FULL_DATA_FILENAME)
        result = runner.run()

        logging.info(f"Part 2: {result}")
