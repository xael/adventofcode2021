# -*- coding: utf-8 -*-
"""Advent of code 2021, day 14

Usage:
   day14.py [--test] [--debug]

Options:
 -h, --help
 -d, --debug
 -t, --test  run against test data
"""


import logging
import sys
import unittest

from docopt import docopt

TEST_DATA_FILENAME = "data/day_14_example.txt"
FULL_DATA_FILENAME = "data/day_14.txt"


class ExtendedPolymerizationReader:
    def __init__(self, filename):
        logging.debug(f"ExtendedPolymerization:init:{filename}")
        with open(filename, "r") as data_file:
            self.data_polymer_template, self.data_pair_insertion = (
                data_file.read().strip().split("\n\n")
            )

    @property
    def pair_insertion(self):
        pairs = {
            pair: element
            for pair, element in [
                line.split(" -> ") for line in self.data_pair_insertion.split("\n")
            ]
        }
        return pairs

    @property
    def polymer_template(self):
        return self.data_polymer_template


class ExtendedPolymerization:
    def __init__(self, filename: str):
        self.instruction = ExtendedPolymerizationReader(filename=filename)

    def run(self, cycles: int = 10) -> int:
        pair_insertion = self.instruction.pair_insertion
        polymer_hash = self.hash_polymer_template(
            polymer_template=self.instruction.polymer_template
        )

        first_letter = self.instruction.polymer_template[0]
        last_letter = self.instruction.polymer_template[-1]

        for _cycle in range(0, cycles):
            new_polymer_hash = polymer_hash.copy()

            for key in polymer_hash:
                if key in pair_insertion:
                    new_polymer_hash[key] -= polymer_hash[key]
                    insert = pair_insertion[key]
                    try:
                        new_polymer_hash[f"{key[0]}{insert}"] += polymer_hash[key]
                    except KeyError:
                        new_polymer_hash[f"{key[0]}{insert}"] = polymer_hash[key]

                    try:
                        new_polymer_hash[f"{insert}{key[1]}"] += polymer_hash[key]
                    except KeyError:
                        new_polymer_hash[f"{insert}{key[1]}"] = polymer_hash[key]

            polymer_hash = {
                x: new_polymer_hash[x]
                for x in new_polymer_hash
                if new_polymer_hash[x] > 0
            }
            logging.debug(polymer_hash)

        count = self.count(polymer_hash, first_letter, last_letter)
        ordered_count = {
            k: v for k, v in sorted(count.items(), key=lambda item: item[1])
        }
        min_key = list(ordered_count.keys())[0]
        max_key = list(ordered_count.keys())[-1]
        return ordered_count[max_key] - ordered_count[min_key]

    @staticmethod
    def count(polymer_hash, first_letter: str, last_letter: str):
        count = {}
        for c in first_letter, last_letter:
            try:
                count[c] += 1
            except KeyError:
                count[c] = 1

        for key in polymer_hash:
            for c in key:
                try:
                    count[c] += polymer_hash[key]
                except KeyError:
                    count[c] = polymer_hash[key]

        for key in count:
            count[key] = int(count[key] / 2)

        return count

    @staticmethod
    def hash_polymer_template(polymer_template: str) -> dict:
        polymer_hash = {}
        last_letter = None
        for c in polymer_template:
            if last_letter is not None:
                key = f"{last_letter}{c}"
                try:
                    polymer_hash[key] += 1
                except KeyError:
                    polymer_hash[key] = 1

            last_letter = c

        return polymer_hash


class ExtendedPolymerizationTestCase(unittest.TestCase):
    def test_example_part_1(self):
        runner = ExtendedPolymerization(filename=TEST_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Test Part 1: {result}")
        assert result == 1588

    def test_example_part_2(self):
        runner = ExtendedPolymerization(filename=TEST_DATA_FILENAME)
        result = runner.run(cycles=40)
        logging.info(f"Test Part 2: {result}")
        assert result == 2188189693529


if __name__ == "__main__":
    arguments = docopt(__doc__)

    if "--debug" in arguments and arguments["--debug"]:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    if "--test" in arguments and arguments["--test"]:
        logging.info("Run tests")
        sys.argv = [sys.argv[0]]
        unittest.main()

    else:
        logging.info("Run main part")

        logging.info("Part 1")
        runner = ExtendedPolymerization(FULL_DATA_FILENAME)
        result = runner.run()
        logging.info(f"Part 1: {result}")

        logging.info("Part 2")
        runner = ExtendedPolymerization(FULL_DATA_FILENAME)
        result = runner.run(cycles=40)

        logging.info(f"Part 2: {result}")
